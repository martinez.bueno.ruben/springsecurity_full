/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.controladors;

import com.dam.SpringCRUD.DAO.UsuariDAO;
import com.dam.SpringCRUD.model.Rol;
import com.dam.SpringCRUD.model.Usuari;
import com.dam.SpringCRUD.paquetVeterinari.serveis.UsuariServices;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ruben
 */
@Controller
@Slf4j
public class UsersController {
    @Autowired 
    private UsuariServices UsuariServices;
    
    @Autowired
    private UsuariDAO usersDao;
    
    
    @GetMapping("/registre")
    public String registre(){
        return "registre";
    }
    
    @GetMapping("/login")
    public String login(){
        return "login";
    }
    
        
    @PostMapping("/registre")
    public String registre2(Usuari user,Rol rol,@RequestParam(name = "seleccioRol")String rolSeleccio){
        UsuariServices.registrarPersona(user, rol, rolSeleccio);
        return "redirect:/login";
    }
    
    @GetMapping("/llistaUsuaris")
    public String llistaUsuaris(Usuari user,Model model){
        List<Usuari> listaUsers = UsuariServices.llistarUsers(user);
        model.addAttribute("usuaris",listaUsers);
        return "llistaUsuaris";
    }
    @GetMapping("/bloquejats")
     public String llistaUsuarisBloquejats(Usuari user,Model model){
        List<Usuari> listaUsers = UsuariServices.getBlockedUsers();
        model.addAttribute("usuaris",listaUsers);
        return "llistaUsuaris";
    }
    
    @GetMapping("/desbloqueja/{id}")
    public String desbloquejarUsuari(@PathVariable Long id,Usuari user){
        UsuariServices.desbloquejarUsuari(id, user);
        return "redirect:/llistaUsuaris";
        
    }
    
}
