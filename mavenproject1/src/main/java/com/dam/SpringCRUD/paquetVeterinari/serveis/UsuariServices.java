/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.serveis;

import com.dam.SpringCRUD.DAO.RolDao;
import com.dam.SpringCRUD.DAO.UsuariDAO;
import com.dam.SpringCRUD.model.Rol;
import com.dam.SpringCRUD.model.Usuari;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ruben
 */
@Service
public class UsuariServices implements UsuariServiceInterface {

    @Autowired
    private UsuariDAO usersDao;

    @Autowired
    private RolDao rolDao;

    @Override
    @Transactional
    public void guardarUser(Usuari usuari) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        usuari.setPassword(passwordEncoder.encode(usuari.getPassword()));
        usuari.setIntents(3);
        this.usersDao.save(usuari);

    }

    @Override
    public void guardarRol(Usuari usuari, Rol rol) {
        rol.setIdUsuari(usuari);
        rolDao.save(rol);
    }

    @Override
    public List<Usuari> llistarUsers(Usuari usuari) {
        return (List<Usuari>) usersDao.findAll();

    }

    @Transactional(readOnly = true)
    public List<Usuari> getBlockedUsers() {
        return usersDao.blockedUsers();
    }

    @Override
    @Transactional(readOnly = true)
    public Usuari getUserById(Long id) {
        return usersDao.findById(id).orElse(null);

    }

    @Override
    public Usuari getUserByUsername(String username) {
        return usersDao.findByUsername(username);
    }

    @Override
    public void registrarPersona(Usuari user, Rol rol, String seleccioRol) {
        guardarUser(user);
        rol.setNom(seleccioRol);
        guardarRol(user, rol);
        System.out.println("User guardado");
    }

    @Override
    public void desbloquejarUsuari(Long id, Usuari user) {
        user = getUserById(id);
        user.setIntents(3);
        usersDao.save(user);
        System.out.println("S'ha desbloquejat el usuari");
    }

}
