/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.serveis;

import com.dam.SpringCRUD.model.Rol;
import com.dam.SpringCRUD.model.Usuari;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Ruben
 */
public interface UsuariServiceInterface{
    public void guardarUser(Usuari usuari);
    public void guardarRol(Usuari usuari,Rol rol);
    public List<Usuari> llistarUsers(Usuari usuari);
    public Usuari getUserById(Long id);
    public Usuari getUserByUsername(String username);
    
    public void registrarPersona(Usuari user,Rol rol,String seleccioRol);
    
    public void desbloquejarUsuari(Long id,Usuari user);
}
