/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.dam.SpringCRUD.DAO;

import com.dam.SpringCRUD.model.Rol;
import com.dam.SpringCRUD.model.Usuari;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Ruben
 */
public interface RolDao extends JpaRepository<Rol,Long>{
    
}
